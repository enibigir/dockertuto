#include <iostream>
#include <fstream>

int main(int argc, char** argv){
  if (argc==3){
    std::ofstream myfile;
    myfile.open(argv[1]);
    std::cout << "Hello world. We will write a message to " << argv[1] << std::endl;
    myfile << "Writting to this file. My message is " << argv[2] << std::endl;
    myfile.close();
  }
  else{
    std::cout << argv[0] << " takes 2 argments:" << std::endl;
    std::cout << "\tfilename and message" << std::endl;
  }
  return 0;
}